import {applyMiddleware, createStore, compose} from 'redux';
import logger from 'react-logger';
import thunk from 'redux-thunk';
import promise from 'redux-promise';

import config from './config';
import reducer from './reducer';

let middleware = null

console.log(`running in ${config.NODE_ENV} environment`)

if ('development','stagind'.indexOf(config.NODE_ENV)>= 0){
    middleware = applyMiddleware(promise, thunk, logger)

}
else{
    middleware = applyMiddleware(promise, thunk)
}

const store = createStore(reducer, compose(middleware))

export default store