import React, { Component} from 'react';
import{Route, Switch, Redirect, withRouter} from 'react-router-dom'
import{connect} from 'react-redux'
import axios from 'axios'
import moment from 'moment'
import * as _ from 'lodash'
// import {Layout} from 'antd'

import List from './components/List';

function App() {
  return (
   <div className="container">
      <div className="row">
        <div className="col-md-6 mx-auto">
          <h1 className="text-center">Products </h1>
          <List />
        </div>
      </div>
   </div>
  );
}

export default App;
