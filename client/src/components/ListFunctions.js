import axios from 'axios'

export const getList = ()=>{
	return axios.get('/api/products',{
		headers: {'Content-Type': 'application/json'}
	})
	.then((res)=>{
		return res.data
	})
}	

export const addItem = (product_code, name, description) =>{
	return axios
	.post('/api/products',{
		product_code: product_code,
		name: name,
		description: description
	},{
		headers: {'Content-Type': 'application/json'}
	})
	.then(res =>{
		console.log(res)
	})
}

export const deleteItem = (id) =>{
	 axios.delete(`/api/products/${id}`,{
		headers: {'Content-Type': 'application/json'}
	})
	.then(res =>{
		console.log(res)
	})
	.catch(err =>{
		console.log(err)
	})
}

export const updateItem = (product_code, name, description,id) =>{
	return axios.put(`/api/products/${id}`,{
		product_code: product_code,
		name: name,
		description: description
		},
		{
		headers: {'Content-Type': 'application/json'}
	})
	.then(res =>{
		console.log(res)
	})
	.catch(err =>{
		console.log(err)
	})
}