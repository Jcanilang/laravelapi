import React, { Component} from 'react';

import {getList, 
		addItem, 
		deleteItem, 
		updateItem
		} from './ListFunctions'

class List extends Component{
	constructor(){
		super()
		this.state = {
			id:'',
			product_code:'',
			name:'',
			description:'',
			editDisabled:false,
			items:[]
		}

		this.onSubmit = this.onSubmit.bind(this)
		this.onChange = this.onChange.bind(this)
	}

	componentDidMount(){
		this.getAll()
	}

	onChange = e =>{
		this.setState({
				[e.target.name]:e.target.value

			})
	}

	getAll = () => {
		getList().then(data=>{
				this.setState({
					product_code:'',
					name:'',
					description:'',
					items:[...data]
				},
				()=>{
					console.log(this.state.items)
				}
				)
			})
	}

	onSubmit = e => {
			e.preventDefault()
			addItem(
				this.state.product_code,
				this.state.name,
				this.state.description
				).then(()=>{
				this.getAll()
			})
			this.setState({
				product_code:'',
				name:'',
				description:''
			})
		}

		onUpdate = e => {
			e.preventDefault()
			updateItem(
				this.state.product_code,
				this.state.name,
				this.state.description, 
				this.state.id)
			.then(()=>{
				this.getAll()
			})
			this.setState({
				product_code:'',
				name:'',
				description:'',
				editDisabled:''
			})
			this.getAll()
		}

		onEdit = (itemid, e) =>{
			e.preventDefault()

			let data = [...this.state.items]
			data.forEach((item, index) =>{
				if(item.id === itemid){
					this.setState({
						id:item.id,
						product_code:item.product_code,
						name:item.name,
						description:item.description,
						editDisabled: true
					})
				}
			})
		}

		onDelete = (val, e)=>{
			e.preventDefault()

			deleteItem(val)
			
				this.getAll()

		}

		render() {
			return(
					<div className="col-md-12">
						<form onSubmit={this.onSubmit}>
							<div className="form-group">
								<label htmlFor= "title">Title</label>
								<div className="row">
									<div className="col-md-12">
										<input type="text"
										className="form-control"
										id="product_code"
										name="product_code"
										value={ this.state.product_code ||'' }
										onChange={this.onChange.bind(this)}
										/>
										
										<input type="text"
										className="form-control"
										id="name"
										name="name"
										value={ this.state.name ||'' }
										onChange={this.onChange.bind(this)}
										/>
									
										<input type="text"
										className="form-control"
										id="description"
										name="description"
										value={ this.state.description ||'' }
										onChange={this.onChange.bind(this)}
										/>
									</div>
								</div>

							</div>
							{!this.state.editDisabled?(
								<button type="submit" 
								className="btn btn-success" 
								onClick={this.onSubmit.bind(this)}
								>Submit</button>
								):(
								''
								)}

								{this.state.editDisabled?(
									<button type="submit" 
									className="btn btn-primary" 
									onClick={this.onUpdate.bind(this)}
									>Update</button>
									):('')}
						</form>

						<table className="table">
							<tbody>	
								{this.state.items.map((item, index)=>(

								<tr key={index}>
									<td className="text-left">{item.product_code}</td>
									<td className="text-left">{item.name}</td>
									<td className="text-left">{item.description}</td>
									<td className="text-right">
								 		<button className="btn btn-info mr-1"
											href=""
											disabled = {this.state.editDisabled}
											onClick={this.onEdit.bind(this, item.id)} 
											>
										Edit
										</button>
										<button className="btn btn-danger"
											href=""
											disabled = {this.state.editDisabled}
											onClick={this.onDelete.bind(this, item.id)}
										>
										Delete
										</button>
									</td>
								</tr>
										
								
								 	))}
							</tbody>
						</table>

					</div>
				)
		}
}

export default List;