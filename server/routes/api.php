<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('register','API\RegisterController@register');
Route::post('login','API\UserController@login');
// Route::middleware('auth:api')->group(function(){
//     Route::resource('products','API\ProductController');
// });

//Article routes
Route::get('articles','API\ArticleController@index');
Route::get('articles/{id}','API\ArticleController@show');
Route::post('articles','API\ArticleController@store');
Route::post('articles/{id}','API\ArticleController@update');
Route::delete('articles/{id}','API\ArticleController@destroy');

//Events routes
Route::get('events','API\EventController@index');
Route::get('events/{id}','API\EventController@show');
Route::post('events','API\EventController@store');
Route::post('events/{id}','API\EventController@update');
Route::delete('events/{id}','API\EventController@destroy');

//Promos routes
Route::get('promos','API\PromoController@index');
Route::get('promos/{id}','API\PromoController@show');
Route::post('promos','API\PromoController@store');
Route::post('promos/{id}','API\PromoController@update');
Route::delete('promos/{id}','API\PromoController@destroy');

    // Product Routes
Route::get('products','API\ProductController@index');
Route::get('products/{id}','API\ProductController@show');
Route::post('products','API\ProductController@store');
Route::put('products/{id}','API\ProductController@update');
Route::delete('products/{id}','API\ProductController@destroy');


// User Routes
    Route::get('users','API\UserController@index');
    Route::get('users/{id}','API\UserController@show');
    Route::post('users','API\UserController@store');
    Route::post('users/{id}','API\UserController@update');
    Route::delete('users/{id}','API\UserController@delete');

// Route::group(['middleware'=>'auth:api'],function(){



// });