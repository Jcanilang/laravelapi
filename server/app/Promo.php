<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
    //

    protected $fillable = ['promo_code','name','details'];

}
