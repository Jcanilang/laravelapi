<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\User;
use Validator;
use Illuminate\Support\Facades\Auth;

class UserController extends BaseController
{
    public $successStatus = 200;
    //
    public function index(){
        $users = User::all();
        return response()->json($users);

    }
    public function show($id){

        $user = User::whereId($id)->get();

        if(is_null($user)){
            return $this->sendError('User not found.');
        }
        
        return response()->json($user);
    }

    public function store(Request $request){
        $input = $request->all();
        $validator = Validator::make($input,[
                'name' => 'required',
                'email'=> 'required|email',
                'password' => 'required',
                // 'c_password'=> 'required|same:password'
            ]);
    
            if($validator->fails()){
                return $this->sendError('Validation Error.',$validator->errors());
            }
    
            
            $input['password'] = bcrypt($input['password']);
            $user = User::create($input);
            $success['token'] = $user->createToken('MyApp')->accessToken;
            $success['name'] = $user->name;
    
           return response()->json($user);
            // return $this->sendResponse($success, 'User created successfully.');

    }
    public function update(Request $request, $id){
        $input = $request->all();
        $validator = Validator::make($input,[
                'name' => 'required',
                'email'=> 'required|email',
                'password' => 'required',
                'c_password'=> 'required|same:password'
            ]);
    
            if($validator->fails()){
                return $this->sendError('Validation Error.',$validator->errors());
            }
            
            $user = User::whereId($id)->update($input);
            $success['name'] = $user->name;

            return response()->json($user);  
    }

    public function delete($id){

        $user = User::whereId($id)->delete();

        return response()->json($user);
    }

    public function login(){ 
        if(Auth::attempt(
            ['email' => request('email'), 
            'password' => request('password')]
            )){ 
           $user = Auth::user(); 
           $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            return response()->json(['success' => $success], $this-> successStatus); 
          } 
          else{ 
           return response()->json(['error'=>'Unauthorised'], 401); 
           } 
        }

        public function logout(Request $request){
            $request->user()->token()->revoke();
            return response()->json('Successfully logout');
        }

}
