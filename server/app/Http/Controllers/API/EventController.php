<?php


namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Event;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;
use Complex\Exception;

class EventController extends BaseController
{
    //

    public function index(){
        try{
            $events = Event::all();

            return response()->json($events);
        }
        catch(Exception $e){
            return response()->json($e);
        }

    }

    public function show($id){
        try{
        $event = Event::whereId($id)->get();
            
        if(is_null($event)){
            return $this->sendError('Promo not found.');
        }

        return response()->json($event);
        }
        catch(Exception $e){
            return response()->json($e);
        }
    }

    public function store(Request $request){
        

        $input = $request->all();

        $validator = Validator::make($input,[
            'event_name' => 'required',
            'details' =>'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try{
            $event = Event::create($input);

            return response()->json($event);
        }
        catch(Exception $e){
            return response()->json($e);
        }
        
    }

    public function update(Request $request,$id){
        $input = $request->all();

        $validator = Validator::make($input,[
            'event_name'=> ' required',
            'details' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validator Error.', $validator->errors());
        }
    try{
            $event = Event::whereId($id)->update($input);

            return response()->json($event);
        }
        catch(Exception $e){
            return response()->json($e);
        }
    }

    public function destroy($id){
        $event = Event::whereId($id)->delete();
        return response()->json($event);
    }
}
