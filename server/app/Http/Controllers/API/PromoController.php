<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Promo;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;

class PromoController extends BaseController
{
    //
    public function index(){
        $promos = Promo::all();
        return response()->json($promos);

    }

    public function store(Request $request){
        $input = $request->all();

        $validator = Validator::make($input,[
            'promo_code' => 'required',
            'name' => 'required',
            'details' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());

        }

        $promo = Promo::create($input);
        return response()->json($promo);

    }

    public function show($id){
        $promo = Promo::whereId($id)->get();
        
            if(is_null($promo)){
                return $this->sendError('Promo not found.');
            }
            
            return response()->json($promo);
    }

    public function update(Request $request, $id){
        $input = $request->all();
        
        $validator = Validator::make($request->all(),[
            'promo_code' => 'required',
            'name'=>'required',
            'details'=>'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.',$validator->errors());
        }

        $promo = Promo::whereId($id)->update($input);

        return response()->json($promo);
    
    }

    public function destroy($id){
        $promo = Promo::whereId($id)->delete();

        return response()->json($promo);
    }
}
