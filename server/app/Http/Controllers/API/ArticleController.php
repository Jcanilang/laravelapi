<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Article;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;
use Complex\Exception;
use App\Http\Utils\ArticleStatus;

class ArticleController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $articles = Article::whereStatus(ArticleStatus::ACTIVE)->get();
        
        return response()->json($articles);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(),[
            'name'=>'required',
            'content' => 'required',
            'category' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try{

            $input = $request->all();
            $article = Article::create($input);
            return response()->json($article);

        }
        catch(Exception $e){
            return response()->json($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try{
            $article = Article::whereId($id)->get();
            return response()->json($article);
        }
        catch(Exception $e){

            return response()->json($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(),[
            'name'=>'required',
            'content' => 'required',
            'category' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try{

            $input = $request->all();
            $article = Article::whereId($id)->update($input);

            return response()->json($article);
        }
        catch(Exception $e){
            return response()->json($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $article = Article::whereId($id)->delete();
        return response()->json($article);

    }
}
