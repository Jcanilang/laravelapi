<?php

namespace App\Http\Utils;

class ArticleStatus
{
    const INACTIVE = 0;
    const ACTIVE = 1;
}