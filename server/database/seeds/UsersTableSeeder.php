<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *admin123
     *user123
     * @return void
     */
    public function run()
    {
        //

        DB::table('users')->delete();
        DB::table('users')->insert(array(
            
            0 => array(
                'id'=>1,
                'name'=>'admin',
                'email' => 'admin@gmail.com',
                'email_verified_at'=>NULL,
                'password' => '$2y$10$MJIqVyeN9dQ5KdKDMB3RAe5D114J.WwVbi2XTlmWVixZmyZhfKgHK',
                // 'is_admin' => 1,
                // 'status' => 1,
                // 'avatar' => 'none',
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => array(
                'id'=>2,
                'name'=>'user',
                'email' => 'user@gmail.com',
                'email_verified_at'=>NULL,
                'password' => '$2y$10$5/MO3MqTF7FPL5mRh42M8.V/cV00ytU68uCyUwklodHV6melv1Hmm',
                // 'is_admin' => 0,
                // 'status' => 1,
                // 'avatar' => 'none',
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
    }
}
